const commonConfig = require('./webpack.common.js')
const { merge } = require('webpack-merge')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')


module.exports = merge(
    commonConfig,
    {
        mode: "production",
        plugins: [
            new CleanWebpackPlugin()
        ]
    }
)