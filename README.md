# 🚀 Tentwenty Assigment Project 🚀

## to build prod version
```
npm run build:prod
```

or

```
yarn build:prod
```

## to serve the application you can use
```
npm run serve
```

or

```
yarn serve
```