import { ACESFilmicToneMapping, PCFSoftShadowMap, PerspectiveCamera, Scene, sRGBEncoding, WebGLRenderer } from "three";
import { animater } from "./animater/animater";


function getCanvasElement() {
    const canvas = document.querySelector("#twenty_main_canvas");
    if (!canvas) {
        throw new Error("There should be a canvas")
    }
    return canvas;
}
export const camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.25, 2000);
export const scene = new Scene();

export const renderer = new WebGLRenderer({
    alpha: true,
    antialias: true,
    canvas: getCanvasElement()
});
renderer.setSize(renderer.domElement.clientWidth, renderer.domElement.clientHeight);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
renderer.shadowMap.enabled = true
renderer.toneMapping = ACESFilmicToneMapping
renderer.toneMappingExposure = 0.5
renderer.outputEncoding = sRGBEncoding
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = PCFSoftShadowMap;

animater.register(() => {
    renderer.render(scene, camera);

})