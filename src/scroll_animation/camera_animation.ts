import gsap from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { camera } from "../consts";


gsap.registerPlugin(ScrollTrigger);


gsap.timeline({
    scrollTrigger: {
        trigger: ".scene-3d",
        start: "top top",
        end: "+=970%",
        pin: true,
        scrub: true,
        onUpdate: function () {
            camera.updateProjectionMatrix();
        }
    }
})
    .to(camera.position, {
        y: -90,
        duration: 10
    },)
