
import { scene } from "../consts";
import { loadModel } from "../utils/loader.utils";
import { initModelByConfig } from "./models-setup.utils";


export const shape0 = await loadModel("fitstShape.glb");
export const shape1 = await loadModel("secondShape.glb");
export const shape2 = await loadModel("thirdShape.glb");
export const shape3 = await loadModel("fourdShape.glb");
export const shape4 = await loadModel("fifthShape.glb");
export const shape5 = await loadModel("sixthShape.glb");
export const shape6 = await loadModel("seventhShape.glb");

const shapes = [
    shape0,
    shape1,
    shape2,
    shape3,
    shape4,
    shape5,
    shape6
];

shapes.map(
    (shape, index) => {
        initModelByConfig(shape, `shape${index}`);
        scene.add(shape.scene);
    }
);
