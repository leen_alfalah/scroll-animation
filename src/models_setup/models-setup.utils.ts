import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import CONFIG from "../models_setup/models-config.json";

const HERO_SECTION_THRESH_HOLD = 14;


export function initModelByConfig(shape: GLTF, name: string) {
    const config = (<any>CONFIG)[name];
    shape.scene.position.set(config.position.x, config.position.y - HERO_SECTION_THRESH_HOLD, config.position.z);
    shape.scene.rotation.set(config.rotation.x, config.rotation.y, config.rotation.z);
    if (config.scale)
        shape.scene.children[0].scale.setScalar(config.scale);
}