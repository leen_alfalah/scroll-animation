
import { shape6 } from "../../../models_setup/models";
import { onMouseMoveFactory } from "../../../mouse_animation/mouse_animation";
import { Component } from "../../base/component.decorator";
import "./custody.component.scss";

@Component({
    selector: "tntwnt-custody",
    template: require("./custody.component.html").default
})
export class CustodyComponent extends HTMLElement {
    onInit() {
        this.addEventListener("mousemove", onMouseMoveFactory(<MouseEvent>event, shape6, "shape6"));
    }

}
