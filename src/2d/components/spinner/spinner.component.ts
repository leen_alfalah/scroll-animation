
import { Component } from "../../base/component.decorator";
import "./spinner.component.scss";


@Component({
    selector: "tntwnt-spinner",
    template: require("./spinner.component.html").default
})
export class SpinnerComponent extends HTMLElement { }
