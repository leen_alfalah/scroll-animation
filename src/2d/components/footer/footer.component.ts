
import { Component } from "../../base/component.decorator";
import "./footer.component.scss";


@Component({
    selector: "tntwnt-footer",
    template: require("./footer.component.html").default
})
export class FooterComponent extends HTMLElement { }
