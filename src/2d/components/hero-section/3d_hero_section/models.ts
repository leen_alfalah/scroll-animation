
import gsap from "gsap";
import { loadModel } from "../../../../utils/loader.utils";
import "./environment_map";
import "./scene_lights";
import { scene } from "./setup";

export let hero = await loadModel("hero_section.glb")
hero.scene.children[0].castShadow = true
hero.scene.children[0].receiveShadow = true
hero.scene.scale.setScalar(5)
hero.scene.position.set(25, -1, -9)
hero.scene.rotation.set(-0.072, -0.202, 0.015)
scene.add(hero.scene)
gsap.from(hero.scene.rotation, { duration: 15, x: 13 },)
