import { Color, DirectionalLight } from "three";
import { scene } from "./setup";

const light_top = new DirectionalLight(new Color("#ffffff"), 1.1);
light_top.position.set(10, 0, 0);
light_top.castShadow = true
light_top.shadow.mapSize.width = 512; // default
light_top.shadow.mapSize.height = 512; // default
light_top.shadow.camera.near = 0.5; // default
light_top.shadow.camera.far = 500; // default
light_top.shadow.bias = - 0.005
scene.add(light_top)


