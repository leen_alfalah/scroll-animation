import { PMREMGenerator } from "three";
import { renderer } from "./setup";


export const generator = new PMREMGenerator(renderer);
generator.compileCubemapShader();