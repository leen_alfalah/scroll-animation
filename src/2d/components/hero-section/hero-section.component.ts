
import { Component } from "../../base/component.decorator";
import "./hero-section.component.scss";


@Component({
    selector: "tntwnt-hero-section",
    template: require("./hero-section.component.html").default
})
export class HeroSectionComponent extends HTMLElement { }
