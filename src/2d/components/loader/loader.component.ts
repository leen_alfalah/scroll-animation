
import { Component } from "../../base/component.decorator";
import "./loader.component.scss";


@Component({
    selector: "tntwnt-loader",
    template: require("./loader.component.html").default
})
export class LoaderComponent extends HTMLElement { }
