
import { shape4 } from "../../../models_setup/models";
import { onMouseMoveFactory } from "../../../mouse_animation/mouse_animation";
import { Component } from "../../base/component.decorator";
import "./investment.component.scss";

@Component({
    selector: "tntwnt-investment",
    template: require("./investment.component.html").default
})
export class InvestmentComponent extends HTMLElement {
    onInit() {
        this.addEventListener("mousemove", onMouseMoveFactory(<MouseEvent>event, shape4, "shape4"));
    }

}
