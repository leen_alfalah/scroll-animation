
import { shape3 } from "../../../models_setup/models";
import { onMouseMoveFactory } from "../../../mouse_animation/mouse_animation";
import { Component } from "../../base/component.decorator";
import "./brokerag.component.scss";

@Component({
    selector: "tntwnt-brokerag",
    template: require("./brokerag.component.html").default
})
export class BrokeragComponent extends HTMLElement {
    onInit() {
        this.addEventListener("mousemove", onMouseMoveFactory(<MouseEvent>event, shape3, "shape3"));
    }
}
