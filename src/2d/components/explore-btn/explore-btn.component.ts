import { Component } from "../../base/component.decorator";
import "./explore-btn.component.scss";


@Component({
    selector: "tntwnt-explore-btn",
    template: require("./explore-btn.component.html").default
})
export class ExploreBtnComponent extends HTMLElement {
    onInit() {
        this.setBackgroundColor();
    }

    isSecondary() {
        return this.hasAttribute("secondary");
    }

    setBackgroundColor() {
        if (this.isSecondary()) {
            (<HTMLButtonElement>this.children.item(0)).setAttribute("secondary", "true");
        }
    }
}
