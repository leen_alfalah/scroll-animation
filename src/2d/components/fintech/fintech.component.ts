
import { shape5 } from "../../../models_setup/models";
import { onMouseMoveFactory } from "../../../mouse_animation/mouse_animation";
import { Component } from "../../base/component.decorator";
import "./fintech.component.scss";

@Component({
    selector: "tntwnt-fintech",
    template: require("./fintech.component.html").default
})
export class FintechComponent extends HTMLElement {
    onInit() {
        this.addEventListener("mousemove", onMouseMoveFactory(<MouseEvent>event, shape5, "shape5"));
    }
}
