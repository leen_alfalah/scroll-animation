
import { shape2 } from "../../../models_setup/models";
import { onMouseMoveFactory } from "../../../mouse_animation/mouse_animation";
import { Component } from "../../base/component.decorator";
import "./distribution.component.scss";

@Component({
    selector: "tntwnt-distribution",
    template: require("./distribution.component.html").default
})
export class DistributionComponent extends HTMLElement {
    onInit() {
        this.addEventListener("mousemove", onMouseMoveFactory(<MouseEvent>event, shape2, "shape2"));
    }

}
