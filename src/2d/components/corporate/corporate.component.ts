import { shape0 } from "../../../models_setup/models";
import { onMouseMoveFactory } from "../../../mouse_animation/mouse_animation";
import { Component } from "../../base/component.decorator";
import "./corporate.component.scss";


@Component({
    selector: "tntwnt-corporate",
    template: require("./corporate.component.html").default
})
export class CorporateComponent extends HTMLElement {
    onInit() {
        this.addEventListener("mousemove", onMouseMoveFactory(<MouseEvent>event, shape0, "shape0"));
    }
}




