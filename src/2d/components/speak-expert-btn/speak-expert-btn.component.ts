
import { Component } from "../../base/component.decorator";
import "./speak-expert-btn.component.scss";


@Component({
    selector: "tntwnt-speak-expert-btn",
    template: require("./speak-expert-btn.component.html").default
})
export class SpeakExpertBtnComponent extends HTMLElement { }
