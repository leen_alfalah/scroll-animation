import { Component } from "../../base/component.decorator";
import "./logo.component.scss";


@Component({
    selector: "tntwnt-logo",
    template: require("./logo.component.html").default
})
export class LogoComponent extends HTMLElement { }
