import { Component } from "../../base/component.decorator";
import "./header.component.scss";

@Component({
    selector: "tntwnt-header",
    template: require("./header.component.html").default
})
export class HeaderComponent extends HTMLElement { }