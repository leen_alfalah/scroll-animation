
import { shape1 } from "../../../models_setup/models";
import { onMouseMoveFactory } from "../../../mouse_animation/mouse_animation";
import { Component } from "../../base/component.decorator";
import "./issuance.component.scss";

@Component({
    selector: "tntwnt-issuance",
    template: require("./issuance.component.html").default
})
export class IssuanceComponent extends HTMLElement {
    onInit() {
        this.addEventListener("mousemove", onMouseMoveFactory(<MouseEvent>event, shape1, "shape1"));

    }
}
