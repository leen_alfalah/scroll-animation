import "./app.component.scss";
import { Component } from "./base/component.decorator";

@Component({
    selector: "tntwnt-app",
    template: require("./app.component.html").default
})
export class AppComponent extends HTMLElement { }