import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { loadManager } from "../loader/loading-manager";


const loader = new GLTFLoader(loadManager);
const PREFIX = "models";


export function loadModel(path: string) {
    const _path = [PREFIX, path].join("/");
    return loader.loadAsync(_path);
}