import { camera, renderer } from "../consts";

export function setup() {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.position.set(0, 5.857867680630245e-16, 15.566623919171994)
}