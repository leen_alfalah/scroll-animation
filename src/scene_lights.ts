import { Color, DirectionalLight } from "three";
import { scene } from "./consts";

const light_top = new DirectionalLight(new Color("#ffffff"), 1);
light_top.position.set(-10, -10, -0);
scene.add(light_top)

const light_left = new DirectionalLight(new Color("#ffffff"), 1);
scene.add(light_left)

const light_right = new DirectionalLight(new Color("#ffffff"), 2);
light_right.position.set(-10, 10, -0);
scene.add(light_right)

const light_bottom = new DirectionalLight(new Color("#ffffff"), 2);
scene.add(light_bottom)