import { PMREMGenerator } from "three";
import { renderer } from "./consts";


export const generator = new  PMREMGenerator(renderer);
generator.compileCubemapShader();