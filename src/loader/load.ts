import { loadManager } from "./loading-manager";


loadManager.onLoad = () => {
    const loaderElement = document.querySelector("tntwnt-loader");
    if (loaderElement)
        document.body.removeChild(loaderElement);
}