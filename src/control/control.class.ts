import { PerspectiveCamera, WebGLRenderer } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { animater } from "../animater/animater";
import { camera, renderer } from "../consts";


export class Control {
    private _control!: OrbitControls;


    constructor(
        private _camera: PerspectiveCamera = camera,
        private _renderer: WebGLRenderer = renderer
    ) {
        this._setupControl();
        this._registerOnAnimater();
    }

    private _setupControl() {
        this._control = new OrbitControls(this._camera, this._renderer.domElement);
        camera.position.set(-21, 34, 4);
        this._control.enableDamping = true
        this._control.dampingFactor = .9;
        this._control.keys = {
            LEFT: "ArrowLeft", //left arrow
            UP: "ArrowUp", // up arrow
            RIGHT: "ArrowRight", // right arrow
            BOTTOM: "ArrowDown" // down arrow
        }
        this._control.listenToKeyEvents(document.body);
    }

    private _registerOnAnimater() {
        animater.register(() => {
            this._control.update();
        });
    }
}