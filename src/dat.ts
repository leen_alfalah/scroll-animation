import { GUI } from "dat.gui";
import { hero } from "./2d/components/hero-section/3d_hero_section/models";

const gui = new GUI();

gui.add(hero.scene.position, "x", 0, 100).step(0.001)
gui.add(hero.scene.position, "y", -100, 0).step(0.001)
gui.add(hero.scene.position, "z", -100, 0).step(0.001)


gui.add(hero.scene.rotation, "x", -1, 1).step(0.001)
gui.add(hero.scene.rotation, "y", -1, 1).step(0.001)
gui.add(hero.scene.rotation, "z", -1, 1).step(0.001)
