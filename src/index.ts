import "./2d";
import "./2d/components/hero-section/3d_hero_section/models";
import { animater } from "./animater/animater";
import "./environment_map";
import "./loader/load";
import "./models_setup/models";
import "./mouse_animation/mouse_animation";
import "./scene_lights";
import "./scroll_animation/camera_animation";
import { setup } from "./utils/setup.utils";


setup();
animater.animate();