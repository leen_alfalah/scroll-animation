import { MathUtils } from "three";
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";
import CONFIG from "../models_setup/models-config.json";


export function onMouseMoveFactory(event: MouseEvent, shape: GLTF, name: string) {
    const config = (<any>CONFIG)[name];
    const x = config.rotation.x;
    const y = config.rotation.y;
    return (event: MouseEvent) => {
        const mouse = { x: 0, y: 0 };
        mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
        shape.scene.rotation.x = MathUtils.lerp(x, (mouse.x), .1)
        shape.scene.rotation.y = MathUtils.lerp(y, (mouse.y), .1);
    }
}