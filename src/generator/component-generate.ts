const fs = require("fs");
const path = require("path");
const config = require("./config");
const getComponentName = require("./component-name.utils.js");
const getFileName = require("./file-name.utils.js");


const args = process.argv.slice(2);

if (args.length < 1) {
    throw new Error(
        "Must Provide Component Name"
    );
}
const componentName = args[0];
const fileName = getFileName(componentName);
const dirPath = args.length > 1 ? args[1] : process.env.INIT_CWD;
fs.lstatSync(dirPath).isDirectory();

const className = getComponentName(componentName);
const selector = [config.prefix, componentName].join("-");

fs.mkdirSync(path.join(dirPath, componentName));
["html", "scss"].map(ext => {
    fs.writeFileSync(path.join(dirPath, componentName, `${fileName}.${ext}`), "");
});

const tsContent = `
import { Component } from "src/2d/base/component.decorator";
import "./${fileName}.scss";


@Component({
    selector: "${selector}",
    template: require("./${fileName}.html").default
})
export class ${className} extends HTMLElement { }
`
fs.writeFileSync(path.join(dirPath, componentName, `${fileName}.ts`), tsContent);